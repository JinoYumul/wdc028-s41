import { useEffect, useState } from 'react'
import moment from 'moment'
import { Tabs, Tab } from 'react-bootstrap'
import MonthlyChart from '../components/MonthlyChart'

export default function insights(){
	const [distances, setDistances] = useState([])
	const [durations, setDurations] = useState([])
	const [amounts, setAmounts] = useState([])

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.travels.length > 0){
				let monthlyDistance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyDuration = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyAmount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

				data.travels.forEach(travel => {
					const index = moment(travel.date).month()

					monthlyDistance[index] += (parseInt(travel.distance)/1000)
					monthlyDuration[index] += (parseInt(travel.duration)/60)
					monthlyAmount[index] += (travel.charge.amount)
				})

				setDistances(monthlyDistance)
				setDurations(monthlyDuration)
				setAmounts(monthlyAmount)
			}
		})
	}, [])

	return(
		<Tabs defaultActiveKey="distances" id="monthlyFigures">
			<Tab eventKey="distances" title="Monthly Distance Travelled">
				<MonthlyChart figuresArray={distances} label={"Monthly total in kilometers"}/>
			</Tab>
			<Tab eventKey="durations" title="Monthly Time Spent Travelling">
				<MonthlyChart figuresArray={durations} label={"Monthly total in minutes"}/>
			</Tab>
			<Tab eventKey="amounts" title="Monthly Travel Expenditures">
				<MonthlyChart figuresArray={amounts} label={"Monthly total in Philippine Pesos"}/>
			</Tab>
		</Tabs>
	)
}