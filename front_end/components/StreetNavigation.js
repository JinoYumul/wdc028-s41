import { useEffect, useRef, useState, useContext } from 'react'
import { Row, Col, Card, Button, Alert } from 'react-bootstrap'
import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY
import MapboxDirections from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions'
import { PayPalButton } from 'react-paypal-button-v2'
import UserContext from '../UserContext'
import Router from 'next/router'

export default function StreetNavigation(){

	const { user } = useContext(UserContext)

    const [distance, setDistance] = useState(0)
    const [duration, setDuration] = useState(0)
    const [originLong, setOriginLong] = useState(0)
    const [originLat, setOriginLat] = useState(0)
    const [destinationLong, setDestinationLong] = useState(0)
    const [destinationLat, setDestinationLat] = useState(0)
    const [isActive, setIsActive] = useState(false)
    const [amount, setAmount] = useState(0)

    const mapContainerRef = useRef(null)

    function recordTravel(chargeId) {
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/travels`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                originLong: originLong,
                originLat: originLat,
                destinationLong: destinationLong,
                destinationLat: destinationLat,
                duration: duration,
                distance: distance,
                amount: amount,
                chargeId: chargeId
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Router.push('/history')
            }else{
                Router.push('/error')
            }
        })
    }

    useEffect(() => {
    	if(distance !== 0 && duration !== 0){
    		setIsActive(true)
    	}else{
    		setIsActive(false)
    	}
    }, [distance, duration])

    useEffect(() => {

    	const map = new mapboxgl.Map({
    		container: mapContainerRef.current,
    		style: 'mapbox://styles/mapbox/streets-v11',
    		center: [121.04382, 14.63289],
    		zoom: 12
    	})

    	const directions = new MapboxDirections({
    		accessToken: mapboxgl.accessToken,
    		unit: 'metric',
    		profile: 'mapbox/driving'
    	})

    	map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

    	map.addControl(directions, 'top-left')

    	directions.on('route', e => {
    		if(typeof e.route !== "undefined"){
    			setOriginLong(e.route[0].legs[0].steps[0].intersections[0].location[0])
    			setOriginLat(e.route[0].legs[0].steps[0].intersections[0].location[1])
    			setDestinationLong(e.route[0].legs[0].steps[e.route[0].legs[0].steps.length-1].intersections[0].location[0])
    			setDestinationLat(e.route[0].legs[0].steps[e.route[0].legs[0].steps.length-1].intersections[0].location[1])
    			setDistance(e.route[0].distance)
    			setDuration(e.route[0].duration)
                setAmount(Math.round(e.route[0].distance/1000)*40)
    		}
    	})

    	return () => map.remove()
    }, [])

	return (
        <Row>
            <Col xs={12} md={8}>
                <div className="mapContainer" ref={mapContainerRef}/>
            </Col>
            <Col xs={12} md={4}>
                {user.id === null
                ?   <Alert variant="info">You must be logged in to record your travels.</Alert>
                :   <Card>
                        <Card.Title>Record Route</Card.Title>
                        <Card.Text>
                            Origin Longitude: {originLong}
                        </Card.Text>
                        <Card.Text>
                            Origin Latitude: {originLat}
                        </Card.Text>
                        <Card.Text>
                            Destination Longitude: {destinationLong}
                        </Card.Text>
                        <Card.Text>
                            Destination Latitude: {destinationLat}
                        </Card.Text>
                        <Card.Text>
                            Total Distance: {Math.round(distance)} meters
                        </Card.Text>
                        <Card.Text>
                            Total Duration: {Math.round(duration/60)} minutes
                        </Card.Text>
                        <Card.Text>
                            Booking Cost: &#8369;{amount}
                        </Card.Text>
                        {isActive === true
                        ? <PayPalButton
                            amount={amount}
                            onSuccess={(details, data) => {
                                setOriginLong(0)
                                setOriginLat(0)
                                setDestinationLong(0)
                                setDestinationLat(0)
                                setDistance(0)
                                setDuration(0)
                                setAmount(0)
                                recordTravel(data.orderID)
                            }}
                            options={{
                                currency: "PHP",
                                clientId: `${process.env.NEXT_PUBLIC_PAYPAL_CLIENT_ID}`
                            }}
                            />
                        : <Alert variant="info">Generate a route to book a ride</Alert>
                        }
                    </Card>
                }
            </Col>
        </Row>
	)
}